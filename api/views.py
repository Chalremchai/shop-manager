from django.shortcuts import render
from .models import Shop,Product,Order,OrderProduct
from rest_framework import viewsets
from .serializer import ShopSerializer,ProductSerializer,OrderProductSerializer,OrderSerializer,ProductCreateSerializer,OrderProductCreateSerializer,OrderCreateSerializer
# Create your views here.

class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_serializer_class(self):
        if self.request.method in ['POST','PUT','PATCH']:
            return ProductCreateSerializer
        return ProductSerializer

class OrderProductViewSet(viewsets.ModelViewSet):
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer

    def get_serializer_class(self):
        if self.request.method in ['POST','PUT','PATCH']:
            return OrderProductCreateSerializer
        return OrderProductSerializer

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_serializer_class(self):
        if self.request.method in ['POST','PUT','PATCH']:
            return OrderCreateSerializer
        return OrderSerializer
    

